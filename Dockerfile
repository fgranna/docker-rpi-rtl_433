FROM sysrun/rpi-rtl-sdr-base:0.4

MAINTAINER Frederik Granna

WORKDIR /tmp

ENV commit_id 729504b1c2835f3acc62835f87fdff78aec39ba8

RUN git clone https://github.com/merbanan/rtl_433.git && \
    cd rtl_433 && \
    git reset --hard $commit_id && \
    mkdir build && \
    cd build && \
    cmake ../ && \
    make && \
    make install && \
    cd / && \
    rm -rf /tmp/rtl_433

WORKDIR /

ENTRYPOINT ["rtl_433"]
